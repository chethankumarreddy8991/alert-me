package com.android.alertme;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private TextView speedTextView;
    private TextView forceTextView;
    private TextView latitudeTextView;
    private long lastUpdate = 0;
    Double maxforce=0.0;

    private TextView longitudeTextView;

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private static final String PREFS_NAME = "MyPrefs";
    private static final String KEY_CONTACT = "contact";
    private static final String KEY_MAX_SPEED = "max_speed";
//    private static final String KEY_MAX_FORCE = "max_force";
//    private static final String PHOTO_URI_KEY = "photoUri";
//    private static final String VIDEO_URI_KEY = "videoUri";

    private boolean smsSent = false;
    private Handler handler = new Handler();
    VideoView videoView;
    private Button settingsButton;
    private ImageView photoImageView;
    private ActivityResultLauncher<Intent> filePickerLauncher;
    private ActivityResultLauncher<Intent> videoPickerLauncher;

    private Uri videoUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        speedTextView = findViewById(R.id.speedTextView);
        latitudeTextView = findViewById(R.id.latitudeTextView);
        longitudeTextView = findViewById(R.id.longitudeTextView);
        settingsButton = findViewById(R.id.settingsButton);
        forceTextView=findViewById(R.id.forceTextView);
        videoView=findViewById(R.id.videoView);
        photoImageView = findViewById(R.id.photoImageView);
        // Initialize the ActivityResultLauncher
        filePickerLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            Uri imageUri = data.getData();
                            loadImage(imageUri);
                        }
                    }
                });
        videoPickerLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                             videoUri = data.getData();
                             //playVideo(videoUri);
                        }
                    }
                });

        // Open the file picker to select an image
        openFilePicker();
        openVideoPicker();
        photoImageView.setVisibility(View.INVISIBLE);
        // Set the Uri to the VideoView
        videoView.setVideoURI(videoUri);
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);

// Set the MediaController to the VideoView
        videoView.setMediaController(mediaController);
        videoView.setVisibility(View.INVISIBLE);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        if (accelerometer == null) {
            Toast.makeText(this, "Accelerometer not available on this device", Toast.LENGTH_LONG).show();
            finish();
        }
        if (checkLocationPermission()) {
            startLocationUpdates();
        }
        lastUpdate = System.currentTimeMillis();
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettingsDialog();
            }
        });
        showUserGuideDialog();
        // Retrieve and display saved settings
        displaySavedSettings();

    }
    private void openFilePicker() {
        // Create an Intent to open the file picker
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        // Start the file picker activity using the ActivityResultLauncher
        filePickerLauncher.launch(intent);
    }
    private void openVideoPicker() {
        // Create an Intent to open the file picker for video
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("video/*");

        // Start the file picker activity using the ActivityResultLauncher
        videoPickerLauncher.launch(intent);
    }

    private void playVideo(Uri videoUri) {
        // Set the video to the VideoView
        videoView.setVideoURI(videoUri);
        videoView.start();
    }
    private void loadImage(Uri imageUri) {
        // Set the image to the ImageView
        photoImageView.setImageURI(imageUri);
    }
    private void showUserGuideDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(getLayoutInflater().inflate(R.layout.dialog_user_guide, null));
        builder.setPositiveButton("Got It", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //videoView.start();
            }
        });
        builder.setCancelable(false); // Prevent dismissing by tapping outside the dialog

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Your accuracy change handling code goes here
    }
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Settings");

        // Set up the input
        final View view = getLayoutInflater().inflate(R.layout.dialog_settings, null);
        builder.setView(view);

        final EditText contactEditText = view.findViewById(R.id.contactEditText);
        final EditText maxSpeedEditText = view.findViewById(R.id.maxSpeedEditText);

        // Set input types
        contactEditText.setInputType(InputType.TYPE_CLASS_PHONE);
        maxSpeedEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        // Load existing values
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        contactEditText.setText(prefs.getString(KEY_CONTACT, ""));
        maxSpeedEditText.setText(String.valueOf(prefs.getFloat(KEY_MAX_SPEED, 0.0f)));

        // Set up the buttons
        builder.setPositiveButton("Save", (dialog, which) -> {
            // Save the input values
            String contact = contactEditText.getText().toString();
            float maxSpeed = Float.parseFloat(maxSpeedEditText.getText().toString());

            // Save to SharedPreferences
            SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
            editor.putString(KEY_CONTACT, contact);
            editor.putFloat(KEY_MAX_SPEED, maxSpeed);
            editor.apply();
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();

    }

    private void displaySavedSettings() {
        // Display saved contact and max speed
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String contact = prefs.getString(KEY_CONTACT, "");
        float maxSpeed = prefs.getFloat(KEY_MAX_SPEED, 0.0f);

        // Use the retrieved values as needed, for example:
        // contactTextView.setText("Contact: " + contact);
        // maxSpeedTextView.setText("Max Speed: " + maxSpeed);
    }
    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission not granted, request it
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted, start location updates
                startLocationUpdates();
            } else {
                // Permission denied, handle accordingly
            }
        }
    }

    private void updateSpeedAndLocation(Location location) {
        // Update the speed display
        float speed = location.getSpeed();
        String speedText = String.format("Speed: %.2f km/h", speed * 3.6);
        speedTextView.setText(speedText);

        // Update latitude and longitude displays
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        String latitudeText = String.format("Latitude: %.6f", latitude);
        String longitudeText = String.format("Longitude: %.6f", longitude);
        latitudeTextView.setText(latitudeText);
        longitudeTextView.setText(longitudeText);
        alert(latitude,longitude,speed);
    }
    private void requestSmsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.SEND_SMS
            ) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{android.Manifest.permission.SEND_SMS},
                        1
                );
            }
        }
    }
    private void alert(double latitude, double longitude, float speed) {
        speed=speed*(float)3.6;
        //KEY_CONTACT,KEY_MAX_SPEED
        // Retrieve saved contact and max speed from SharedPreferences
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String contact = prefs.getString(KEY_CONTACT, "");
        float maxSpeed = prefs.getFloat(KEY_MAX_SPEED, 0.0f);
        // Check if speed is reaching max speed
        if (speed >= maxSpeed) {
            // Build the SMS message
            String message = "Warning: You are exceeding the maximum speed of " + maxSpeed + " km/h. " +
                    "Location: https://maps.google.com/maps?q=" + latitude + "," + longitude;

            // Check if contact is provided
            if (!TextUtils.isEmpty(contact)) {
                // Send SMS
                requestSmsPermission();
                sendSMS(contact, message);
            } else {
                // Handle the case where no contact is provided
                Toast.makeText(this, "No contact specified. Cannot send SMS.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void sendSMS(String phoneNumber, String message) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNumber, null, message, null, null);
            Toast.makeText(this, "SMS sent successfully.", Toast.LENGTH_SHORT).show();
            videoView.setVisibility(View.VISIBLE);
            photoImageView.setVisibility(View.VISIBLE);
            playVideo(videoUri);
        } catch (Exception e) {
            Toast.makeText(this, "Failed to send SMS.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            videoView.setVisibility(View.VISIBLE);
            photoImageView.setVisibility(View.VISIBLE);
            playVideo(videoUri);
        }
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String contact = prefs.getString(KEY_CONTACT, "");
        float max_acc = 5.0f;

// Inside your sensor event listener method
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastUpdate > 100) {
                lastUpdate = currentTime;
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                float acceleration = Math.abs((float) (Math.sqrt(x * x + y * y + z * z) / SensorManager.GRAVITY_EARTH));

                // Display acceleration on the screen
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        forceTextView.setText("Acceleration: " + acceleration);

                    }
                });

                // Check acceleration condition and send SMS if necessary
                if (acceleration > max_acc && !smsSent) {
                    sendSMS(contact, "Possibility of crash, acceleration is: " + acceleration);
                    smsSent = true; // Set the flag to true after sending SMS

                    // Schedule a task to reset the flag after 15 seconds
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            smsSent = false;
                        }
                    }, 15000); // 15 seconds in milliseconds
                }
            }
        }

    }


    private void startLocationUpdates() {
        // Create location request
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000); // Update every 1 second
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Create location callback
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    updateSpeedAndLocation(location);
                }
            }
        };

        // Request location updates
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    @Override
    protected void onDestroy() {
        // Stop location updates when the activity is destroyed
        if (fusedLocationClient != null && locationCallback != null) {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        }
        super.onDestroy();
    }
}
